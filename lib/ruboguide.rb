# frozen_string_literal: true

require_relative 'ruboguide/version'

module Ruboguide
  class Error < StandardError; end
end
