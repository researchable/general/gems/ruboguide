# 1.0.0 (2023-03-08)


### Bug Fixes

* rubocop-performance version ([5684302](https://gitlab.com/researchable/general/gems/ruboguide/commit/568430292457654ee9ba34bf1e5efb7938056c4a))


### Features

* first implementation ([261ff73](https://gitlab.com/researchable/general/gems/ruboguide/commit/261ff7351dc71e907be10bba32480130dbbb93bd))
