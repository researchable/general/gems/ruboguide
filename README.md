# Ruboguide

Ruboguide is Researchable's gem to standardize rubocop configuration, to achieve a uniform style on all ruby projects.

## Installation

Add the following to your Gemfile:
```ruby
gem 'ruboguide', '~> 1.0'
```

If you already have rubocop installed, remove it. This gem already installs rubocop for you (and ensures all projects
use the same version of rubocop).

Edit your `rubocop.yml` to include the following:

```yaml
inherit_gem:
  ruboguide:
    - styles/rubocop.yml
```

If your project is a rails project you should instead add:
```yaml
inherit_gem:
  ruboguide:
    - styles/rubocop-rails.yml
```

## Usage

This gem gives you rubocop. To lint your codebase, call it the way you normally would e.g.:
```shell
rubocop -a
```

To run linting on CI using this gem, you can use the CI template available on the 
[templates/gitlab/base](https://gitlab.com/researchable/general/templates/gitlab/base/-/tree/master/ruby) repository

## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake spec` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and the created tag, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## License

The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).
