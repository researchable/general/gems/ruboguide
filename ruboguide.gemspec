# frozen_string_literal: true

require_relative 'lib/ruboguide/version'

Gem::Specification.new do |spec|
  spec.name = 'ruboguide'
  spec.version = Ruboguide::VERSION
  spec.authors = ['Researchable']
  spec.email = ['info@researchable.nl']

  spec.summary = "Researchable's rubocop style guide"
  spec.description = 'Ruboguide is all you need to add rubocop to your project. It standardizes the rubocop version '\
                     'and the cops configurations.'
  spec.homepage = 'http://researchable.nl'
  spec.license = 'MIT'
  spec.required_ruby_version = '>= 3.0'

  # spec.metadata["allowed_push_host"] = "https://example.com"

  spec.metadata['homepage_uri'] = spec.homepage
  spec.metadata['source_code_uri'] = 'https://gitlab.com/researchable/general/gems/ruboguide'
  spec.metadata['changelog_uri'] = 'https://gitlab.com/researchable/general/gems/ruboguide/-/blob/master/README.md'

  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added into git.
  spec.files = Dir.chdir(__dir__) do
    `git ls-files -z`.split("\x0").reject do |f|
      (f == __FILE__) || f.match(%r{\A(?:(?:bin|test|spec|features)/|\.(?:git|travis|circleci)|appveyor)})
    end
  end
  spec.bindir = 'exe'
  spec.executables = spec.files.grep(%r{\Aexe/}) { |f| File.basename(f) }
  spec.require_paths = ['lib']

  # Uncomment to register a new dependency of your gem
  spec.add_dependency 'rubocop', '= 1.23.0'
  spec.add_dependency 'rubocop-performance', '= 1.12.0'
  spec.add_dependency 'rubocop-rails', '= 2.12.4'
  spec.add_dependency 'rubocop-rake', '= 0.6.0'
  spec.add_dependency 'rubocop-rspec', '= 2.6.0'

  # For more information and examples about making a new gem, check out our
  # guide at: https://bundler.io/guides/creating_gem.html
  spec.metadata = {
    'rubygems_mfa_required' => 'true'
  }
end
